import React, { Component } from 'react';
import { BITBUCKET_API_URL } from './api';
import './App.css';
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: ''
    };
    this.Interval = '';
  }
  componentDidMount = () => {
    if (this.state.token === '') {
      this.Interval = setInterval(() => {
        console.log(localStorage.getItem('token'));
        console.log(
          localStorage.getItem('token') !== undefined &&
          localStorage.getItem('token') !== ''
        );
        if (
          localStorage.getItem('token') !== undefined &&
          localStorage.getItem('token') !== '' &&
          localStorage.getItem('token') !== null
        ) {
          clearInterval(this.Interval);

          this.setState({ token: localStorage.getItem('token') }, () => {
            this.props.history.push('bitbucket/');
          });
        }
      }, 1000);
    }
  };

  AccessBucket = () => {
    //this.props.history()
    window.open(BITBUCKET_API_URL, '', 'height:calc(100vh - 10px)');
    // fetch(BITBUCKET_API_URL)
    //   .then(res => res.json())
    //   .then(
    //     result => {
    //       //console.log(result);
    //     },
    //     error => {
    //       //this.setState({error});
    //     }
    //   );
  };
  render() {
    return (
      <div className="App vh-100 overflow-scroll d-flex justify-content-center align-items-center">
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Static Hosting</h1>
        </header> */}
        <div>
          <h4>DepZ</h4>
          {/* <h4> Continuous Deployment</h4> */}
          <p> Select the BitBucket Repo. We can deploy the site in your own Amazon S3 Bucket.
            When you push to Git, Pipelines will automatically deploy the result.
              </p>
          {/* <p>Select the Git provider where your site’s source code is hosted. When you push to Git, we run your build tool of choice on our servers and deploy the result.</p> */}


          <button
            type="button"
            onClick={this.AccessBucket}
            className="mt-3 btn btn-primary"
          >
            BitBucket
          </button>
        </div>
      </div>
    );
  }
}

export default App;
