import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="p-3 text-center">
                Made With <span role="img" aria-label="heart">❤️</span> by <a href="https://twitter.com/ibalajisankar">Balaji Sankar</a>
            </footer>
        );
    }
}

export default Footer;
