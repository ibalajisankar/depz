import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import App from './App';
import Footer from './footer';
import Bitbucket from './pages/bitbucket';
import TokenRedirect from './pages/Redirect';
export default class Main extends Component {
  render() {
    return (
      <main>

        <Switch>
          <Route exact path="/" component={App} />
          <Route exact path="/bitbucket" component={Bitbucket} />
          <Route exact path="/redirect" component={TokenRedirect} />
        </Switch>
        <Footer />

      </main>

    );
  }
}
