import React, { Component } from 'react';

export default class TokenRedirect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: 'Window Will Close Automatically'
    };
  }
  componentDidMount() {
    const access_token_string = this.props.location.hash.split('&scope');
    const access_token = access_token_string[0].split('#access_token=');

    localStorage.setItem('token', decodeURIComponent(access_token[1]));
    window.close();
  }
  render() {
    return <div className="vh-100 d-flex justify-content-center align-items-center">
      <div>{this.state.data}</div>
    </div>
  }
}
