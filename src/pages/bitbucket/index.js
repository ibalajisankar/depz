import { Icon, Steps } from 'antd';
import React, { Component } from 'react';
import BranchList from './branch';
import RepoList from './repo';
const Step = Steps.Step;
let GETRequestOptions = {
  method: 'GET',
  headers: {
    // 'Content-Type': 'application/json',
    Authorization: `Bearer ${localStorage.getItem('token')}`
  }
};
export default class Bitbucket extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      current: 1,
      username: '',
      repo: [],
      selectedRepo: [],
      branch: { values: [] },
      loading: true
    };
  }
  next = index => {
    this.setState({ selectedRepo: this.state.repo.values[index] }, () => {
      this.fetchBranch(this.state.selectedRepo.branch_slug.href);
    });
    const current = this.state.current + 1;
    this.setState({ current });
  };

  prev = () => {
    const current = this.state.current - 1;
    this.setState({ current });
  };
  componentDidMount() {
    GETRequestOptions.headers.Authorization = `Bearer ${localStorage.getItem('token')}` //${authToken}`,
    fetch('https://api.bitbucket.org/2.0/user', GETRequestOptions)
      .then(res => res.json())
      .then(
        result => {
          if (result.type !== 'error') {
            this.setState({ user: result, username: result.username });
            fetch(
              `https://api.bitbucket.org/2.0/teams?role=admin`,
              GETRequestOptions
            )
              .then(res => res.json())
              .then(TeamResult => {
                console.log(TeamResult);
                if (TeamResult.values.length) {
                  this.setState({ username: TeamResult.values[0].username });
                }
                this.fetchRepo(
                  'https://api.bitbucket.org/2.0/repositories?pagelen=10&role=member'
                );
              });
          } else {
            localStorage.removeItem('token');
          }
        },
        error => {
        }
      );
  }
  fetchBranch = url => {
    this.setState({ loading: true });
    fetch(url + '?pagelen=100', GETRequestOptions)
      .then(res => res.json())
      .then(result => {
        const branchData = result.values.map(data => {
          return {
            name: data.name
          };
        });
        const branch = {
          next: result.next,
          values: branchData
        };
        this.setState({ branch, loading: false });
      });
  };
  fetchRepo = url => {
    this.setState({ loading: true });
    fetch(url, GETRequestOptions)
      .then(res => res.json())
      .then(result => {
        const repoData = result.values.map(data => {
          return {
            updated_on: data.updated_on,
            is_private: data.is_private,
            projectName: data.project !== undefined ? data.project.name : '',
            size: data.size,
            name: data.name,
            repo_slug: data.full_name,
            branch_slug: data.links.branches
          };
        });
        const repo = {
          next: result.next,
          values: repoData
        };
        this.setState({ repo, loading: false });
      });
  };
  handleChange = value => {
    console.log(`selected ${value}`);
  };
  render() {
    const steps = [
      {
        title: 'Git Provider',
        content: 'First-content'
      },
      {
        title: "Repo's list",
        description: '',
        content: (
          <div className="d-flex flex-column pt-5">
            <RepoList
              data={this.state.repo.values}
              loading={this.state.loading}
              next={this.next}
            />
            {this.state.repo.values.length !== 0 && (
              <Icon
                className="align-self-end mt-3"
                style={{ fontSize: 30, color: 'black' }}
                onClick={() => {
                  this.fetchRepo(this.state.repo.next);
                }}
                type="right-circle"
              />
            )}
          </div>
        )
      },
      {
        title: 'Build & AWS',
        description: '',
        content: (
          <BranchList
            data={this.state.branch.values}
            handleChange={this.handleChange}
            selectedRepo={this.state.selectedRepo}
          />
        )
      }
    ];
    return (
      <div className="vh-100 d-flex justify-content-center overflow-scroll" style={{ background: 'rgba(0, 0, 0, 0.9)' }}>
        <div className="col-6 mx-auto card p-4 overflow-scroll m-5">
          <div>
            <Steps progressDot current={this.state.current}>
              {steps.map(item => (
                <Step key={item.title} title={item.title} />
              ))}
            </Steps>
            <div className="steps-content">
              {steps[this.state.current].content}
            </div>
            <div className="steps-action">

            </div>
          </div>
        </div>
      </div>
    );
  }
}
