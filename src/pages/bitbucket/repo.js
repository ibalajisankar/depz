import { Avatar, Icon, List } from 'antd';
import moment from 'moment';
import React, { Component } from 'react';
export default class RepoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: 'Window Will Close Automatically'
    };
  }
  render() {
    return (
      <List
        itemLayout="horizontal"
        loading={this.props.loading}
        dataSource={this.props.data}
        renderItem={(item, index) => (
          <List.Item onClick={() => this.props.next(index)}>
            <List.Item.Meta
              avatar={
                <Avatar src="https://d301sr5gafysq2.cloudfront.net/f0465896a301/img/repo-avatars/default.png" />
              }
              title={item.name}
              description={`${item.projectName} - ${moment(
                item.updated_on
              ).fromNow()}`}
            />
            <div />
            {item.is_private && (
              <div className="d-flex">
                <svg
                  width="10px"
                  height="13px"
                  viewBox="0 0 10 13"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g
                    id="Page-1"
                    stroke="none"
                    strokeWidth="1"
                    fill="none"
                    fillRule="evenodd"
                  >
                    <g id="icon" fill="#000000" fillRule="nonzero">
                      <path
                        d="M9.38916667,6.02375 L8.75208333,6.02375 L8.75208333,3.90041667 C8.75208333,2.84166667 8.35,1.87083333 7.6875,1.16208333 C7.02666667,0.454166667 6.08416667,-0.0025 5.04583333,1.85034347e-16 C4.00666667,-0.0025 3.06416667,0.454166667 2.40416667,1.16208333 C1.74125,1.87083333 1.33916667,2.84166667 1.33916667,3.90041667 L1.33916667,6.02375 L0.70125,6.02375 C0.439166667,6.02375 0.224166667,6.23708333 0.224166667,6.50083333 L0.224166667,12.11375 C0.224166667,12.3791667 0.439166667,12.5920833 0.70125,12.5920833 L9.38958333,12.5920833 C9.65416667,12.5920833 9.86791667,12.37875 9.86791667,12.11375 L9.86791667,6.50083333 C9.86791667,6.23708333 9.65375,6.02375 9.38916667,6.02375 Z M3.09833333,3.90041667 C3.09833333,3.29 3.33125,2.74791667 3.69,2.36458333 C4.05125,1.98041667 4.52333333,1.75916667 5.04583333,1.75916667 C5.5675,1.75916667 6.03958333,1.98041667 6.4,2.36458333 C6.76041667,2.74791667 6.99166667,3.29 6.99166667,3.90041667 L6.99166667,6.02375 L3.09833333,6.02375 L3.09833333,3.90041667 Z"
                        id="Shape"
                      />
                    </g>
                  </g>
                </svg>
                <Icon
                  type="right"
                  style={{ marginLeft: 10, fontSize: 16, fontWeight: 900 }}
                />
              </div>
            )}
          </List.Item>
        )}
      />
    );
  }
}
