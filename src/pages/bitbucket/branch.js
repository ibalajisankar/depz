import { Button, Input, message, Select, Skeleton } from 'antd';
import React, { Component } from 'react';
const Option = Select.Option;
const GETRequestOptions = {
  method: 'GET',
  headers: {
    // 'Content-Type': 'application/json',
    Authorization: `Bearer ${localStorage.getItem('token')}` //${authToken}`,
  }
};
export default class BranchList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      href: '',
      setting: {
        branch: 'master',
        buildCmd: '',
        publishDir: '',
        AWS: ''
      },
      loading: false
    };
  }
  onInputChange = (key, e) => {
    let stateChange = { ...this.state };
    stateChange.setting[key] = e.target.value;
    this.setState({ setting: stateChange.setting });
  };
  handleChange = key => {
    let stateChange = { ...this.state };
    stateChange.setting.branch = key;
    this.setState({ setting: stateChange.setting });
  };
  buttonClick = () => {
    this.setState({ loading: true })
    fetch(
      `https://api.bitbucket.org/2.0/repositories/${
      this.props.selectedRepo.repo_slug
      }/commits/${this.state.setting.branch}?pagelen=2`,
      GETRequestOptions
    )
      .then(res => res.json())
      .then(BranchResult => {
        fetch(
          `https://api.bitbucket.org/2.0/repositories/${
          this.props.selectedRepo.repo_slug
          }/src/${BranchResult.values[0].hash}/bitbucket-pipelines.yml`,
          GETRequestOptions
        )
          .then(res => {
            if (res.status !== 404) {
              return res.text();
            } else return res.json();
          })
          .then(SrcResult => {
            const AWSpolicy = `{
  "Version":"2008-10-17",
  "Statement":[{
    "Sid":"PublicReadForGetBucketObjects",
        "Effect":"Allow",
      "Principal": {
            "AWS": "*"
         },
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::${this.state.setting.AWS}/*"
      ]
    }
  ]
}`;
            const AWSpipelines = `
   pipelines:
    default:
          - step:
             image: atlassian/pipelines-awscli
             script:
                - aws s3api create-bucket --bucket ${
              this.state.setting.AWS
              } --create-bucket-configuration LocationConstraint=eu-west-1
                - aws s3 website s3://${this.state.setting.AWS}/ --index-document index.html --error-document index.html
                - aws s3api put-bucket-policy --bucket ${this.state.setting.AWS} --policy file://awspolicy.json
`;
            let pipelines = '';
            if (SrcResult.type !== undefined && SrcResult.type === 'error') {
              pipelines = `
   pipelines:
    branches:
       ${this.state.setting.branch}:
            - step:
                    image: node:latest
                    caches:
                        - node
                    script:
                        - npm install
                        - ${this.state.setting.buildCmd}  
                        - cd ${this.state.setting.publishDir}
                    artifacts:
                        - ${this.state.setting.publishDir}/**
            - step: 
                    image: atlassian/pipelines-awscli
                    script:
                     - aws s3 sync --delete build s3://${
                this.state.setting.AWS
                }/ 
                `;
            }
            else {
              if (SrcResult.includes(this.state.setting.branch)) {
                let branchSplit = SrcResult.split(this.state.setting.branch)
                let colonSplit = branchSplit[1].split(":")
                let space = "";
                space = space.padStart(colonSplit[1].indexOf(colonSplit[1].trim().charAt(0)) - 1, " ");
                let pipelineStep = `
- step:
        image: node:latest
        caches:
            - node
        script:
            - npm install
            - ${this.state.setting.buildCmd}  
            - cd ${this.state.setting.publishDir}
        artifacts:
            - ${this.state.setting.publishDir}/**
- step: 
        image: atlassian/pipelines-awscli
        script:
         - aws s3 sync --delete build s3://${
                  this.state.setting.AWS}/    
                `;
                const appendStep = pipelineStep.split('\n').map(data => space.concat(data)).join("\n");
                colonSplit[1] = appendStep + colonSplit[1];
                branchSplit[1] = colonSplit.join(':');
                pipelines = branchSplit.join(this.state.setting.branch)
              }
              else {
                let defaultFlag = false;
                let branchSplit = SrcResult.split('branches')
                if (branchSplit.length !== 2) {
                  defaultFlag = true;
                  branchSplit = SrcResult.split('default')
                }
                let colonSplit = branchSplit[1].split(":")
                let space = "";
                let defaultSpace = 0;
                if (defaultFlag) {
                  defaultSpace = space.padStart(branchSplit[0].split('\n')[2].length, " ")
                }
                space = space.padStart(colonSplit[1].indexOf(colonSplit[1].trim().charAt(0)) - 1, " ");
                let pipelineStep = `
branches:
   ${this.state.setting.branch}:
        - step:
                image: node:latest
                caches:
                    - node
                script:
                    - npm install
                    - ${this.state.setting.buildCmd}  
                    - cd ${this.state.setting.publishDir}
                artifacts:
                    - ${this.state.setting.publishDir}/**
        - step: 
                image: atlassian/pipelines-awscli
                script:
                 - aws s3 sync --delete build s3://${
                  this.state.setting.AWS
                  }/
`;
                if (branchSplit.length !== 2) {
                  pipelineStep = pipelines.split('branches:')[1];
                }
                const appendStep = pipelineStep.split('\n').map(data => { if (data !== "branches:") { return space.concat(data) } return defaultSpace.concat(data) }).join("\n");
                if (!defaultFlag) { colonSplit[1] = appendStep + colonSplit[1]; }
                else {
                  colonSplit[colonSplit.length - 1] = appendStep + colonSplit[colonSplit.length - 1]
                }
                branchSplit[1] = colonSplit.join(':')
                pipelines = defaultFlag ? branchSplit.join('default') : branchSplit.join('branches')

              }
            }
            var formData = new FormData();
            formData.append('/bitbucket-pipelines.yml', AWSpipelines);
            formData.append('/awspolicy.json', AWSpolicy)
            formData.append('message', 'AWS Pipeline Integration');
            formData.append('branch', this.state.setting.branch);
            const postRequestOptions = {
              method: 'POST',
              headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
              },
              body: formData
            };

            fetch(
              `https://api.bitbucket.org/2.0/repositories/${
              this.props.selectedRepo.repo_slug
              }/src`,
              postRequestOptions
            ).then(res => {
              if (res.status === 201) {
                const StatusInterval = setInterval(() => {
                  fetch(`https://api.bitbucket.org/2.0/repositories/${
                    this.props.selectedRepo.repo_slug
                    }/pipelines/?pagelen=1&sort=-created_on`, GETRequestOptions)
                    .then(res => res.json())
                    .then(res => {
                      if (res.values[0].state.name === "COMPLETED") {
                        clearInterval(StatusInterval);

                        if (res.values[0].state.result.name === "FAILED") {
                          message.error('Bucket Already Exist');
                          this.setState({ loading: false })

                          pipelines = SrcResult;
                        }
                        else {
                          message.success('Bucket Created !!');

                        }
                      }
                    })
                }, 8000);

                setTimeout(() => {
                  var formData = new FormData();

                  formData.append('/bitbucket-pipelines.yml', pipelines);
                  formData.append('message', 'Bitbucket Pipeline Integration');
                  formData.append('branch', this.state.setting.branch);
                  const postRequestOptions = {
                    method: 'POST',
                    headers: {
                      Authorization: `Bearer ${localStorage.getItem('token')}` //${authToken}`,
                    },
                    body: formData
                  };
                  fetch(
                    `https://api.bitbucket.org/2.0/repositories/${
                    this.props.selectedRepo.repo_slug
                    }/src`,
                    postRequestOptions
                  ).then(res => {
                    setTimeout(() => {
                      if (res.status === 201) {
                        this.setState({ loading: false })
                        this.setState({ href: `http://${this.state.setting.AWS}.s3-website.eu-west-1.amazonaws.com` })
                      }
                    }, 5000);

                  });
                }, 20000);

              }
            });
          });
      });
  };
  render() {
    return (
      <React.Fragment>
        {this.state.loading === false &&
          <div className="mt-5">
            <div> {this.state.href}</div>
            <h5 className="mb-3"> Branch to Deploy</h5>
            <Select
              defaultValue="master"
              style={{ width: 120 }}
              onChange={this.handleChange}
            >
              {this.props.data.map((data, index) => {
                return (
                  <Option key={index} value={data.name}>
                    {data.name}
                  </Option>
                );
              })}
            </Select>
            <div className="mt-4 mb-4">
              <h5>Basic Build Settings</h5>
              <Input
                className="mt-2"
                placeholder="Build Command"
                onChange={this.onInputChange.bind(this, 'buildCmd')}
              />
              <Input
                className="mt-2"
                placeholder="Publish Directory"
                onChange={this.onInputChange.bind(this, 'publishDir')}
              />
            </div>
            <div className="mt-2 mb-2">
              <h5 className="mb-3">AWS setting</h5>
              <Input
                placeholder="AWS Bucket Name"
                onChange={this.onInputChange.bind(this, 'AWS')}
              />
              <span> Bucket name must be same as domain name</span>
            </div>
            <Button className="mt-2" type="primary" onClick={this.buttonClick}>
              Done
        </Button>
          </div>
        }
        {
          this.state.loading === true &&
          <div>
            <h5> Deploying ...</h5>
            <Skeleton active />
          </div>
        }
      </React.Fragment>
    );
  }
}
