# DepZ
DepZ is an open source platform for hosting static websites.

As of now all the site are deployed in S3 Bucket from Bitbucket Repo 

### Framework
  1. React

### Components
  1. Ant Design

### Auth Setting
For Bitbucket OAuth create an OAuth consumer in bitbucket

1. Bucketsetting -> OAuth -> Add Consumer
2. Provide the name, description, call back URL.
3. Check the this is private consumer.
4. In permission Tab, Click on 
	1. Read
		1. Account
		2. Team Membership
		3. Project
		4. Snippets
		5. Pipelines
	2. Write
		1. Read 
		2. Repositories
		3. Snippets  
	3. Admin
		1. Repositories   
	4. Email
		1. Account
			 
Save the setting, 
4. Copy the OAuth Key and paste in the api.js file
 https://bitbucket.org/site/oauth2/authorize?client_id={key}&response_type=token

### AWS setting
*AWS IAM*

AWS Services -> Security, Identity & Compliance -> IAM
Click on User and add new user
Create new group and provide S3 access
Add user to that group an access key for that user will be generated.
Store the access keys.

###  Bitbucket Setting
*BitBucket Repo*

-> Login to BitbBucket 
-> Goto Bucket settings -> Environment Variables
-> Add AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY
		
 Deploy the site 😈